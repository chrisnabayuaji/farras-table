<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "rpn_rsia_permata";

// Create connection
$conn = new mysqli($servername, $username, $password, $database);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$data = $_POST;

$sql = "SELECT obat_id, obat_nm FROM mst_obat WHERE obat_nm LIKE '" . @$data['term'] . "%' ORDER BY obat_nm LIMIT 50";
$result = $conn->query($sql);

$res = [];

if ($result->num_rows > 0) {
  // output data of each row
  while ($row = $result->fetch_assoc()) {
    $r = [
      $row['obat_id'],
      $row['obat_nm'],
    ];
    array_push($res, $r);
  }
} else {
  echo "0 results";
}
$conn->close();

echo json_encode($res);
