(function ($) {
  "use strict";

  $.fn.farrasTable = function (options, callback) {
    var settings = $.extend(
      {
        width: "100%",
        columns: [],
        widthColumns: [],
        classColumns: [],
        rows: [],
        focusAfterAdd: 0,
      },
      options
    );

    $(this).addClass("table table-bordered table-sm");

    // initial header
    var header = "<thead><tr>";
    for (var i = 0; i <= settings.columns.length - 1; i++) {
      header =
        header +
        "<th width='" +
        settings.columns[i].width +
        "' class='text-center'>" +
        settings.columns[i].name +
        "</th>";
    }
    header = header + "</tr></thead>";
    this.append(header);

    //initial rows
    var bodyHtml = $('<tbody id="farrastable_body"></tbody>');
    this.append(bodyHtml);
    if (settings.rows.length > 0) {
      settings.rows.forEach((row, krow) => {
        addRow(row, krow);
      });
    }

    addRow();

    function addRow(arr = [], rowIndex = null) {
      var rowHtml = $('<tr id="farrastable_row_' + krow + '"></tr>');
      var krow = rowIndex;
      if (rowIndex == null) {
        krow = settings.rows.length;
      }
      var rowsArray = [];
      for (let i = 0; i < settings.columns.length; i++) {
        var cellHtml = $("<td></td>");
        var value = "";
        if (arr[i] != undefined) {
          value = arr[i];
        }
        var cellContentHtml = $(
          '<input class="w-100" id="farrastable_cell_' +
            krow +
            "_" +
            i +
            '" data-row="' +
            krow +
            '" data-col="' +
            i +
            '" value="' +
            value +
            '">'
        );
        cellHtml.append(cellContentHtml);
        rowHtml.append(cellHtml);
        cellContentHtml.keyup(function (e) {
          onkeyup(this);
        });
        cellContentHtml.keydown(function (e) {
          onkeydown(this, e);
        });
        if (settings.columns[i].type == "text") {
          cellContentHtml.change(function (e) {
            onchange(i, this);
          });
        }
        if (settings.columns[i].type == "autocomplete") {
          farrascomplete(cellContentHtml, i);
        }

        rowsArray.push(value);
      }
      if (arr.length === 0) {
        settings.rows.push(rowsArray);
      }
      bodyHtml.append(rowHtml);
      $("#farrastable_cell_" + krow + "_" + settings.focusAfterAdd).focus();
    }

    function onchange(i, obj) {
      if ($.isFunction(settings.columns[i].onchange)) {
        settings.columns[i].onchange.call(obj);
      }
    }

    function onkeyup(obj) {
      var krow = $(obj).data("col");
    }

    function onkeydown(obj, e) {
      var krow = $(obj).data("row");
      var kcol = $(obj).data("col");
      if (e.which == 13) {
        if (
          kcol === settings.columns.length - 1 &&
          krow === settings.rows.length - 1
        ) {
          addRow();
        } else {
          var nextcol = kcol + 1;
          if (kcol === settings.columns.length - 1) {
            nextcol = 0;
            krow += 1;
          }
          if (settings.columns[kcol].nextCol !== undefined) {
            nextcol = settings.columns[kcol].nextCol;
          }

          setTimeout(() => {
            $("#farrastable_cell_" + krow + "_" + nextcol)
              .focus()
              .select();
          }, 100);
        }
      }
    }

    function farrascomplete(obj, index) {
      if (settings.columns[index].autocomplete.multipleColumn !== undefined) {
        settings.columns[index].autocomplete.multipleColumn.forEach((v, k) => {
          $(obj).data("val" + k, "0");
        });
      }
      obj.on("input", function (e) {
        var currentFocus;
        var a,
          b,
          i,
          val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
          return false;
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "farrascomplete-list");
        a.setAttribute("class", "farrascomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);

        if (settings.columns[index].autocomplete.remoteType == "static") {
          //populate item
          if (settings.columns[index].autocomplete.type == "single") {
            for (
              i = 0;
              i < settings.columns[index].autocomplete.data.length;
              i++
            ) {
              if (
                settings.columns[index].autocomplete.data[i][1]
                  .substr(0, val.length)
                  .toUpperCase() == val.toUpperCase()
              ) {
                b = document.createElement("DIV");
                b.classList.add("farrascomplete-item");
                /*make the matching letters bold:*/
                b.innerHTML =
                  "<strong>" +
                  settings.columns[index].autocomplete.data[i][1].substr(
                    0,
                    val.length
                  ) +
                  "</strong>";
                b.innerHTML += settings.columns[index].autocomplete.data[
                  i
                ][1].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML +=
                  "<input type='hidden' value='" +
                  settings.columns[index].autocomplete.data[i][1] +
                  "'>";
                b.addEventListener("click", function (e) {
                  /*insert the value for the autocomplete text field:*/
                  $(obj).val(this.getElementsByTagName("input")[0].value);
                  /*close the list of autocompleted values,
                          (or any other open lists of autocompleted values:*/
                  if ($.isFunction(settings.columns[index].onchange)) {
                    settings.columns[index].onchange.call(obj);
                  }
                  closeAllLists();
                });
                a.appendChild(b);
              }
            }
          }

          if (settings.columns[index].autocomplete.type == "multiple") {
            b = document.createElement("DIV");
            b.setAttribute("id", this.id + "farrascomplete-list");
            b.setAttribute("class", "farrascomplete-items");
            var c = document.createElement("table");
            c.style.width = settings.columns[index].autocomplete.multipleWidth;
            c.classList.add("table");
            c.classList.add("table-bordered");
            c.classList.add("table-striped");
            c.classList.add("table-sm");
            var d = document.createElement("thead");
            var e = document.createElement("tr");
            var f = "";
            settings.columns[index].autocomplete.multipleColumn.forEach(
              (autocol, autokey) => {
                f += "<th>" + autocol + "</th>";
              }
            );
            e.innerHTML = f;
            d.appendChild(e);
            c.appendChild(d);
            b.appendChild(c);
            a.appendChild(b);
            // row table
            var g = document.createElement("tbody");
            var i = [];
            settings.columns[index].autocomplete.data.forEach(
              (rowVal, rowKey) => {
                if (
                  rowVal[1].substr(0, val.length).toUpperCase() ==
                  val.toUpperCase()
                ) {
                  var tr = document.createElement("tr");
                  tr.classList.add("farrascomplete-table-row");
                  var j = "";
                  rowVal.forEach((colVal, colKey) => {
                    j +=
                      "<td>" +
                      "<strong>" +
                      colVal.substr(0, val.length) +
                      "</strong>" +
                      colVal.substr(val.length) +
                      "</td>";
                  });
                  tr.innerHTML = j;
                  rowVal.forEach((colVal, colKey) => {
                    tr.innerHTML +=
                      "<input type='hidden' value='" + colVal + "'>";
                  });
                  tr.addEventListener("click", function (e) {
                    // /*insert the value for the autocomplete text field:*/
                    $(obj).val(
                      this.getElementsByTagName("input")[
                        settings.columns[index].autocomplete.selectedIndex
                      ].value
                    );
                    settings.columns[index].autocomplete.multipleColumn.forEach(
                      (v, k) => {
                        $(obj).data(
                          "val" + k,
                          this.getElementsByTagName("input")[k].value
                        );
                      }
                    );
                    if ($.isFunction(settings.columns[index].onchange)) {
                      settings.columns[index].onchange.call(obj);
                    }
                    // /*close the list of autocompleted values,
                    //         (or any other open lists of autocompleted values:*/
                    closeAllLists();
                  });
                  i.push(tr);
                }
              }
            );
            i.forEach((el) => {
              g.appendChild(el);
            });
            c.appendChild(g);
            b.appendChild(c);
            a.appendChild(b);
          }
        }

        if (settings.columns[index].autocomplete.remoteType == "ajax") {
          if (settings.columns[index].autocomplete.type == "single") {
            $.ajax({
              type: "post",
              url: settings.columns[index].autocomplete.ajaxUrl,
              dataType: "json",
              data: "term=" + val,
              success: function (data) {
                if (data != null) {
                  data.forEach((item, key) => {
                    b = document.createElement("DIV");
                    b.classList.add("farrascomplete-item");
                    /*make the matching letters bold:*/
                    b.innerHTML =
                      "<strong>" + item[1].substr(0, val.length) + "</strong>";
                    b.innerHTML += item[1].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML +=
                      "<input type='hidden' value='" + item[1] + "'>";
                    b.addEventListener("click", function (e) {
                      /*insert the value for the autocomplete text field:*/
                      $(obj).val(this.getElementsByTagName("input")[0].value);
                      /*close the list of autocompleted values,
                          (or any other open lists of autocompleted values:*/
                      if ($.isFunction(settings.columns[index].onchange)) {
                        settings.columns[index].onchange.call(obj);
                      }
                      closeAllLists();
                    });
                    a.appendChild(b);
                  });
                }
              },
            });
          }
          if (settings.columns[index].autocomplete.type == "multiple") {
            $.ajax({
              type: "post",
              url: settings.columns[index].autocomplete.ajaxUrl,
              dataType: "json",
              data: "term=" + val,
              success: function (data) {
                b = document.createElement("DIV");
                b.setAttribute("id", this.id + "farrascomplete-list");
                b.setAttribute("class", "farrascomplete-items");
                var c = document.createElement("table");
                c.style.width =
                  settings.columns[index].autocomplete.multipleWidth;
                c.classList.add("table");
                c.classList.add("table-bordered");
                c.classList.add("table-striped");
                c.classList.add("table-sm");
                var d = document.createElement("thead");
                var e = document.createElement("tr");
                var f = "";
                settings.columns[index].autocomplete.multipleColumn.forEach(
                  (autocol, autokey) => {
                    f += "<th>" + autocol + "</th>";
                  }
                );
                e.innerHTML = f;
                d.appendChild(e);
                c.appendChild(d);
                b.appendChild(c);
                a.appendChild(b);
                // row table
                var g = document.createElement("tbody");
                var i = [];
                data.forEach((item, key) => {
                  var tr = document.createElement("tr");
                  tr.classList.add("farrascomplete-table-row");
                  var j = "";
                  item.forEach((colVal, colKey) => {
                    j +=
                      "<td>" +
                      "<strong>" +
                      colVal.substr(0, val.length) +
                      "</strong>" +
                      colVal.substr(val.length) +
                      "</td>";
                  });
                  tr.innerHTML = j;
                  item.forEach((colVal, colKey) => {
                    tr.innerHTML +=
                      "<input type='hidden' value='" + colVal + "'>";
                  });
                  tr.addEventListener("click", function (e) {
                    // /*insert the value for the autocomplete text field:*/
                    $(obj).val(
                      this.getElementsByTagName("input")[
                        settings.columns[index].autocomplete.selectedIndex
                      ].value
                    );
                    settings.columns[index].autocomplete.multipleColumn.forEach(
                      (v, k) => {
                        $(obj).data(
                          "val" + k,
                          this.getElementsByTagName("input")[k].value
                        );
                      }
                    );
                    if ($.isFunction(settings.columns[index].onchange)) {
                      settings.columns[index].onchange.call(obj);
                    }
                    // /*close the list of autocompleted values,
                    //         (or any other open lists of autocompleted values:*/
                    closeAllLists();
                  });
                  i.push(tr);
                  //
                });
                i.forEach((el) => {
                  g.appendChild(el);
                });
                c.appendChild(g);
                b.appendChild(c);
                a.appendChild(b);
              },
            });
          }
        }

        /*execute a function presses a key on the keyboard:*/
        obj.on("keydown", function (e) {
          var x = document.getElementById(this.id + "farrascomplete-list");
          if (settings.columns[index].autocomplete.type == "single") {
            if (x) x = x.getElementsByTagName("div");
          }
          if (settings.columns[index].autocomplete.type == "multiple") {
            if (x) x = x.getElementsByTagName("tr");
          }
          if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
              increase the currentFocus variable:*/
            // if (currentFocus >= x.length) currentFocus = 0;
            // else currentFocus++;
            currentFocus += 1;
            /*and and make the current item more visible:*/
            addActive(x);
          } else if (e.keyCode == 38) {
            //up
            /*If the arrow UP key is pressed,
              decrease the currentFocus variable:*/
            // if (currentFocus < 0) currentFocus = x.length - 1;
            // else currentFocus--;
            currentFocus -= 1;
            /*and and make the current item more visible:*/
            addActive(x);
          } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
              /*and simulate a click on the "active" item:*/
              setTimeout(() => {
                if (x) x[currentFocus].click();
              }, 100);
            }
            // $(this).unbind("keydown");
          }
        });

        function addActive(x) {
          /*a function to classify an item as "active":*/
          if (!x) return false;
          /*start by removing the "active" class on all items:*/
          removeActive(x);
          if (currentFocus >= x.length) currentFocus = 0;
          if (currentFocus < 0) currentFocus = x.length - 1;
          /*add class "farrascomplete-active":*/
          x[currentFocus].classList.add("farrascomplete-active");
        }

        function removeActive(x) {
          /*a function to remove the "active" class from all autocomplete items:*/
          for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("farrascomplete-active");
          }
        }

        function closeAllLists(elmnt) {
          /*close all autocomplete lists in the document,
          except the one passed as an argument:*/
          var x = document.getElementsByClassName("farrascomplete-items");
          for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != obj) {
              x[i].parentNode.removeChild(x[i]);
            }
          }
        }

        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
          closeAllLists(e.target);
        });
      });
    }
  };
})(jQuery);
